import Vue from 'vue'
import Vuex from 'vuex'
import L from 'leaflet'
Vue.use(Vuex)
Vue.use(require('vue-shortkey'))

export default new Vuex.Store({
  state: {
    //4 mape na raspolaganju
    Maps: [
      {
        architects: [],
        architectures: []
      },
      {
        architects: [],
        architectures: []
      },
      {
        architects: [],
        architectures: []
      },
      {
        architects: [],
        architectures: []
      }
    ],
    selectedMap: 0,
    tooltipVisible: false,
    overlays : { 
      viewArchitect: false,
      addArchitect: false,
      viewArchitecture: false,
      addArchitecture: false,
      architectTable: false,
      architectureTable: false,
      import: false,
      export: false,
      tutorial: false,
      docs: false
    },
    tutorial: {
      enabled: false,
      type: 0,
      step: 0,
      entityId: ""
    },
    snackbar: {
      visible: false,
      text: null,
      color: "",
      multiline: false
    },
    tutorialbar: {
      visible: false,
      text: null,
      color: "info",
      multiline: true,
      timeout: 60000
    },
    markerMoved: {
      startedMoving: false,
      finishedMoving: true,
      oldCoords: {
        lat: 0,
        lng: 0
      },
      newCoords: {
        lat: 0,
        lng: 0
      },
      id: 0
    },
    icons: {
      // icon for architects
      architects: new L.Icon({
        iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      }),
      // icon for architecture
      architecture: new L.Icon({
        iconUrl: 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-gold.png',
        shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
      }),
      // to be added - highlight icons?
    },
    architects: [],
    architectEdit: -1,
    architectureEdit: -1,
    predefinedPicture: "https://i.imgur.com/yp7O5TV.jpg",
    purposes: [],
    periods: ['Prehistory', 
              'Ancient Mesopotamia', 
              'Classical antiquity',
              'Byzantine',
              'Early Christianity',
              'Romanesque art',
              'Gothic art',
              'Renaissance',
              'Baroque',
              'Classicism',
              'Modernism',
              'Postmodernism'],
    protection: ['UNSECO', 'UN', 'ICOM', 'ICCROM', 'ICOMOS'],
    //UNESO - United Nations Educational, Scientific and Cultural Organization
    //ICOM - The International Council of Museum
    //UN - United nations
    //ICCROM - International Centre for the Study of the Preservation and Restoration of Cultural Property
    //ICOMOS - The international Council of Monuments and Sites
    architectures: [],
    paths: [],
  },
  mutations: {
    editPredefinedPicture(state, data) {
      state.predefinedPicture = data;
    },
    editOverlay(state, data) {
      state.overlays[data.name] = data.value;
    },
    editTutorial(state, data) {
      state.tutorial[data.name] = data.value;
    },
    editTooltip(state) {
      state.tooltipVisible = !state.tooltipVisible;
    },
    changeSelectedMap(state, data) {
      state.selectedMap = data.selectedMap;
    },
    // snackbar
    showSnackbar(state, payload) {
      state.snackbar.text = payload.text
      state.snackbar.multiline = (payload.text.length > 50) ? true : false
      
      if (payload.multiline) {
        state.snackbar.multiline = payload.multiline
      }
      state.snackbar.color = payload.color

      state.snackbar.visible = true
    },
    closeSnackbar(state) {
      state.snackbar.visible = false
      state.snackbar.multiline = false
      state.snackbar.text = null
    },
    // tutorial snackbar
    showTutbar(state, payload) {
      state.tutorialbar.visible = false;
      state.tutorialbar.text = payload.text;      
      if (payload.timeout != undefined) {
        state.tutorialbar.timeout = payload.timeout;
      }
      state.tutorialbar.visible = true;
    },
    closeTutbar(state) {
      state.tutorialbar.visible = false
      state.tutorialbar.text = null
    },
    // architects
    addArchitect(store, architect) {
      var newArchitect = {
        name: architect.name,
        surname: architect.surname,
        birthPlace: architect.birthPlace,
        nationality: architect.nationality,
        dateOfBirth: architect.dateOfBirth,
        dateOfDeath: architect.dateOfDeath,
        picture: architect.picture,
        id: architect.id,
        show: false,
        latLng: {
          lat: architect.latLng.lat,
          lng: architect.latLng.lng
        } 
      }
      store.Maps[store.selectedMap].architects.push(newArchitect);
    },
    addArchitecture(store, architecture) {
      var newArchitecture = {
        name: architecture.name,
        architect: architecture.architect,
        description: architecture.description,
        location: architecture.location,
        constructionDate: architecture.constructionDate,
        epoch: architecture.epoch,
        protection: architecture.protection,
        purpose: architecture.purpose,
        worldWonder: architecture.worldWonder,
        picture: architecture.picture,
        id: architecture.id,
        show: false,
        latLng: {
            lat: architecture.latLng.lat,
            lng: architecture.latLng.lng
        }
      }
      store.Maps[store.selectedMap].architectures.push(newArchitecture);
    },
    copyArchitect(store, data) {
      var newArchitect = Object.assign({}, data.architect);
      store.Maps[data.map].architects.push(newArchitect);
    },
    copyArchitecture(store, data) {
      var newArchitecture = Object.assign({}, data.architecture);
      store.Maps[data.map].architectures.push(newArchitecture);
    },
    toggleShow (store, architect) {
      for (var a of store.architects) {
        if (a.id == architect.id) {
          architect.show = !architect.show;
          break;
        }
      }
    },
    setEditArchitect(store, value) {
      store.architectEdit = value;
    },
    setEditArchitecture(store, value) {
      store.architectureEdit = value;
    },
    editArchitect(store, architect) {
      var index = store.Maps[store.selectedMap].architects.findIndex(a => a.id == architect.id);
      if (index != -1) {
        store.markerMoved.oldCoords.lat = architect.latLng.lat;
        store.markerMoved.oldCoords.lng = architect.latLng.lng;
        console.log(store.markerMoved.oldCoords.lat + " " + store.markerMoved.oldCoords.lng);
        Object.assign(store.Maps[store.selectedMap].architects[index], architect);
      }
      else
        alert("Id changed");
    },
    deleteArchitect(store, architect) {
      var index = store.Maps[store.selectedMap].architects.findIndex(a => a.id == architect.architect.id);
      if (index != -1) {
        var check = store.Maps[store.selectedMap].architectures.findIndex(a => a.architect == store.Maps[store.selectedMap].architects[index].id);
        console.log(check);
        if (check != -1) {
          store.snackbar.text = "Unable to delete! Architect has architectures on this map!";
          store.snackbar.color = "error";
          store.snackbar.visible = true;
          return;
        } else {
          store.Maps[store.selectedMap].architects.splice(index, 1);
          store.snackbar.text = "Successfully deleted architect.";
          store.snackbar.color = "success";
          store.snackbar.visible = true;
        }
      }
    },
    deleteArchitecture(store, architecture) {
      var index = store.Maps[store.selectedMap].architectures.findIndex(a => a.id == architecture.architecture.id);
      if (index != -1) {
        store.Maps[store.selectedMap].architectures.splice(index, 1);
        store.snackbar.text = "Successfully deleted architecture.";
        store.snackbar.color = "success";
        store.snackbar.visible = true;
      }
    },
    editArchitecture(store, architecture) {
      var index = store.Maps[store.selectedMap].architectures.findIndex(a => a.id == architecture.id);
      if (index != -1) {
        store.markerMoved.oldCoords.lat = architecture.latLng.lat;
        store.markerMoved.oldCoords.lng = architecture.latLng.lng;
        Object.assign(store.Maps[store.selectedMap].architectures[index], architecture);
      }
      else
        alert("Id changed");

    },
    finishMarkerMove(store, data) {
      var index = store.Maps[store.selectedMap].architects.findIndex(a => a.id == data.id);
      if (index != -1) {
        store.markerMoved.startedMoving = false;
        store.markerMoved.finishedMoving = true;
        Vue.set(store.Maps[store.selectedMap].architects[index], 'birthPlace', data.city);
      } else {
        index = store.Maps[store.selectedMap].architectures.findIndex(a => a.id == data.id);
        if (index != -1) {
          store.markerMoved.startedMoving = false;
          store.markerMoved.finishedMoving = true;
          Vue.set(store.Maps[store.selectedMap].architectures[index], 'location', data.city);
        } else {
          alert("Not found");
        }
      }
    },
    cancelMarkerMove(store, data) {
      var index = store.Maps[store.selectedMap].architects.findIndex(a => a.id == data.id);
      if (index != -1) {
        store.markerMoved.startedMoving = false;
        Vue.set(store.Maps[store.selectedMap].architects[index], 'latLng', {lat: data.lat, lng: data.lng});
        store.markerMoved.finishedMoving = true;
      } else {
        index = store.Maps[store.selectedMap].architectures.findIndex(a => a.id == data.id);
        if (index != -1) {
          store.markerMoved.startedMoving = false;
          Vue.set(store.Maps[store.selectedMap].architectures[index], 'latLng', {lat: data.lat, lng: data.lng});
          store.markerMoved.finishedMoving = true;
        } else {
          alert("Not found");
        }
      }
    },
    // architect edited mutations (for changing location with dragging)
    startMarkerMove(store, data) {
      store.markerMoved.startedMoving = data.startedMoving;
      store.markerMoved.finishedMoving = data.finishedMoving;
      store.markerMoved.oldCoords.lat = data.oldCoords.lat;
      store.markerMoved.oldCoords.lng = data.oldCoords.lng;
      store.markerMoved.newCoords.lat = data.newCoords.lat;
      store.markerMoved.newCoords.lng = data.newCoords.lng;
      store.markerMoved.id = data.id;
    },
    updateMarkerMove(store, data) {
      store.markerMoved.newCoords.lat = data.lat;
      store.markerMoved.newCoords.lng = data.lng;
    },
    updateMarkerMoveFinished(store, data) {
      store.markerMoved.finishedMoving = data;
    },
    importData(store, data) {
        //importuj na okej mapu
        store.Maps[store.selectedMap].architects = data.data.architects;
        store.Maps[store.selectedMap].architectures = data.data.architectures;
    }
  },
  getters: {
    selectedMap: state => {
      return state.selectedMap;
    },
    periods: state => {
      return state.periods;
    },
    protection: state => {
      return state.protection;
    },
    editedArchitect: state => {
      var obj = state.Maps[state.selectedMap].architects.find(a => a.id == state.architectEdit);
      return obj;
    },
    activeMap: state => {
      return state.Maps[state.selectedMap];
    },
    editedArchitecture: state => {
      var obj = state.Maps[state.selectedMap].architectures.find(a => a.id == state.architectureEdit);
      return obj;
    },
    getArchitectById: (state) => (id) => {
      if(id == "")
        return "";
      var result = state.Maps[state.selectedMap].architects.find(a => a.id == id);
      if (result != undefined)
        return result.name + " " + result.surname;
      return "";
    },
    filterArchitects: (state) => (criteria) => {
      if (criteria == "")
        return state.Maps[state.selectedMap].architects;
      criteria = criteria.toLowerCase();
      var results = state.Maps[state.selectedMap].architects.filter(a => 
        a.name.toLowerCase().includes(criteria) ||
        a.surname.toLowerCase().includes(criteria) || 
        a.birthPlace.toLowerCase().includes(criteria) ||
        a.nationality.toLowerCase().includes(criteria) ||
        a.dateOfBirth.toLowerCase().includes(criteria) ||
        a.dateOfDeath.toLowerCase().includes(criteria)         
      )
      return results;
    },
    filterArchitectures: (state) => (criteria) => {
      if (criteria == "")
        return state.Maps[state.selectedMap].architectures;
      else {
        criteria = criteria.toLowerCase();
        var results = state.Maps[state.selectedMap].architectures.filter(a => 
          a.name.toLowerCase().includes(criteria) ||
          //this.getters.getArchitectById(a.architect).toLowerCase().includes(criteria) ||
          a.description.toLowerCase().includes(criteria) ||
          a.location.toLowerCase().includes(criteria) ||
          a.constructionDate.toLowerCase().includes(criteria) ||
          a.epoch.toLowerCase().includes(criteria) ||
          a.protection.toLowerCase().includes(criteria) || 
          a.purpose.toLowerCase().includes(criteria)  
          //a.worldWonder.toLowerCase().includes(criteria) 
        )
        return results;
      }
    },
    path: (state) => (item) => {
      var architectureLatLng = Object.assign([], [item.latLng.lat, item.latLng.lng]);
      var architect = state.Maps[state.selectedMap].architects.find(a => a.id == item.architect);
      var architectLatLng = Object.assign([], [architect.latLng.lat, architect.latLng.lng]);
      var paths = Object.assign([],[architectureLatLng, architectLatLng]);
      console.log(paths);
      return paths;
    },
    architects: (state) => {
        return state.Maps[state.selectedMap].architects;
    },
    architectures: (state) => {
        return state.Maps[state.selectedMap].architectures;
    },
    tutorialStep: (state) => (type, step) => {
        // vraca true ako je tut enabled i ovo NIJE trenutni korak tutorijala 
        // ovo se koristi za disable svih stvari sto ne treba da se klikne
        if (state.tutorial.enabled && state.tutorial.type != type ||
          state.tutorial.enabled && state.tutorial.type == type && state.tutorial.step != step) {
          return true;
        } else 
        return false;
    },
    editingStep: (state) => (type, step) => {
        // vraca true ako je tut enabled i ovo JESTE trenutni korak tutorijala 
        // ovo se koristi za bojanje dugmica koji trebaju da se kliknu
        if (state.tutorial.enabled && state.tutorial.type == type && state.tutorial.step == step) {
          return true;
        } else 
        return false;
    },
    existsOnMap: (state) => (obj, map, isArchitect) => {
      if (isArchitect) {
        var index = state.Maps[map].architects.findIndex(a => a.id == obj.id);
        if (index == -1)
          return false;
        else 
          return true;
      } else {
        var indx = state.Maps[map].architectures.findIndex(a => a.id == obj.id);
        if (indx != -1)
          return true;
        // jel postoji taj arhitekta na mapi
        var aIndex = state.Maps[map].architects.findIndex(a => a.id == obj.architect);
        if (aIndex != -1)
          return false;
        else
          return true;
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
